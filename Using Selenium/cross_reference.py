from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd

#Create the driver object and open it
driver = webdriver.Firefox(executable_path="./geckodriver.exe")

#Naviguate to the specific address (1er site "Mizuno")
driver.get("https://www.mizunoshop.fr/sports/running/homme/chaussures.html")

response = driver.page_source

html_soup = BeautifulSoup(response, 'html.parser')

product_containers=html_soup.find_all("li",class_="item")

prod_names, prices, tags = [], [], []

for container in product_containers:
    # Obtention des informations de chaque article
    prod_names.append(container.h2.a.text)
    prices.append(container.find("span",{"class":"price"}).text.split("€")[0])
    tags.append(container.div.span.text)
    
#Ajout dans un dataframe
data = pd.DataFrame({"names": prod_names, "prices": prices, "tags":tags})

#Indication de la source Mizuno
data["source"] = "Mizuno"

#Aller au second site
driver.get("https://www.boutiquemarathon.com/slider-de-la-home/route-chemin.html?limit=25&mode=list")

#On remarque qu'au delà des 4 premières pages, il n'y a plus de chaussures. Mais tu peux aller au delà :-)
nb_pages = 4

#Création du dataframe contenant tous les articles du site "boutique marathon"
data2 = pd.DataFrame(columns={"names", "prices", "tags"})

#Faire une boucle pour scraper les articles sur chaque page
for num_page in range(1, nb_pages+1):
    
    #Obtention du code HTML de la page concernée
    response = driver.page_source

    #Recherche des articles via BeautifulSoup
    html_soup = BeautifulSoup(response, 'html.parser')
    product_containers = html_soup.find_all("div",class_="f-fix")
    
    #Listes qui contiendront les informations sur les articles
    prod_names, prices, tags = [], [], []

    for item in product_containers:
        prices.append(int(item.find("span",class_="price").text.split(",")[0]))
        prod_names.append(item.h2.text)
        
        #Balise non précise sur la disponibilité d'un article. On essaye de voir s'il est en stock sinon il est épeuisé.
        try:
            #Ajout de "Stcock" si l'article est encore en stock
            tags.append(item.find("p", class_="stock").text)
        except:
            #Sinon l'article est épuisé
            tags.append("Épuisé")
            
    
    new_data = pd.DataFrame({"names": prod_names, "prices": prices, "tags":"En stock"})
    
    #Indiquer la source de provenance des informations
    new_data["source"] = "boutique_marathon"
    
    #Concaténer la novelle base à l'ancienne
    data2 = data2.append(new_data, sort=False)
    
    #Essayer d'aller à la page suivante sinon indiquer qu'il n'y a plus de pages et arrêter la boucle !
    try:
        driver.find_element_by_css_selector("a.next.i-next").click()
    except:
        print("Il n'y a plus de pages suivantes.")
        
        break #Arrêt de la boucle et fin du programme
        

#Close the driver
driver.close()

#Sauvegarde de la grosse base de données contenant toutes les informations ("Mizuno" et "boutique marathon")
data.append(data2, sort=False).to_csv("Database.csv", sep=";", index = False)

####################### Fin de la première partie #######################

####################### Seconde partie : Cross referencing ###################

#Fonction de Cross-Referencing entre les 2 sites
def cross_reference(title):
    """Cette fonction permet de retourner le tableau contenant la différence des prix entre les 2 sites
    
    Arguments:
        title {[str]} -- [nom de l'article à rechercher]
    
    Returns:
        [pandas dataframe] -- [description]
    """

    #Nouveau dataframe pour sélectionner les articles de la première base
    new_data = data[data.names == title].copy()
    
    #Vérifier l'apparition du titre recheché dans la 2e base de données
    appearances = [title in prod_name for prod_name in data2.names.values]
    
    #Sélectionner les articles où le titre apparait (tu peux printer pour voir les caractéristiques du même article sur les 2 sites :-))
    selected_references = new_data.append(data2[appearances], sort=False)
    #print(selected_references)

    #Dataframe final contenant la différence des prix
    final_data = pd.DataFrame()
    final_data["prod_name"] = [title]
    final_data["Diff"] = [int(selected_references.head(1).prices.values[0]) - int(selected_references.tail(1).prices.values[0])]
    
    #Retouner le dataframe final
    return final_data

#Sélection de tous les noms d'article collectés sur le site "Mizuno"
titles = data.names.values

#Création de la table de données
df = pd.DataFrame(columns={'prod_name', 'Diff'})
for title in titles:
    df = df.append(cross_reference(title), sort=False)

df.to_csv("Cross_Reference_Data.csv", sep=";", index=False)

### FIN ###
