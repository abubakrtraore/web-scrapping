import requests
import re 
import pickle
import pandas as pd
import numpy as np

import statsmodels.api as sm #librairie pour la regression

#librairies pour les graphique
from plotly.offline import iplot, init_notebook_mode
import plotly.graph_objs as go

init_notebook_mode(connected=True)


def get_page_source_content(url):
    """Retourne le code source de la page dont l'url est donnée en entrée
    
    Arguments:
        url {str} -- Lien URL du site où sont scrapées les articles
    
    Returns:
        str -- L'url de l'article concerné
    """
    return requests.get(url).text

def get_links(content):
    """Retourne une liste contenant tous les urls des articles trouvés sur la page "SurfShop" à partir du contenu
    
    Arguments:
        content {str} -- Contenu html de la page "SurfShop"
    
    Returns:
        list -- Liste exhaustive des urls
    """
    pattern = '<a class="product_manu_link" href="(.*)" item'
    output = re.findall(pattern, content)
    print('First link : ', output[0])
    return output

def save_html_contents(all_links, file_name):
    """Sauvegarde tous les codes sources des liens des articles donnés dans la liste en entrée
    
    Arguments:
        all_links {list} -- Liste contenant les urls des articles
        file_name {str} -- Nom qu'il faut donner au fichier à sauvegarder
    """

    html_contents = []
    for url in all_links:
        html_contents.append(get_page_source_content(url))
    
    #save the html object
    with open(file_name,'wb') as file:
        pickle.dump(html_contents, file)

def get_thickness(page_content):
    """Retourne l'epaisseur de l'article concerné à partir de son code source.
       Pour les cas particuliers des articles qui n'ont pas l'épaisseur indiquée
       au bon endroit, la fonction retourne une valeur vide. Ces valeurs seront 
       plus tard supprimés de la base finale pour éviter les valeurs manquantes.
    
    Arguments:
        page_content {str} -- page source d'un article.
    
    Returns:
        int -- Taille en millimètre de l'habit concerné.
    """
    try: #
        return int(re.findall('<p class="noh">(.*)</p>', page_content)[0][0])
    except:
        return None

def get_price(page_content):
    """Retourne le prix d'un article 
       Pour les cas particuliers des articles qui n'ont pas le prix indiqué
       au bon endroit, la fonction retourne une valeur vide. Ces valeurs seront 
       plus tard supprimés de la base finale pour éviter les valeurs manquantes.
    
    Arguments:
        page_content {str} -- page source d'un article de ProShop.
    
    Returns:
        float -- Le prix en euro de l'article
    """
    try:
        return float(re.findall('<span class="price" id="prod(.*) itemprop="price">\n(.*)<',page_content)[0][1].strip()[:5].replace(',','.'))
    except:
        return None


####################### Partie 1 #############################################
# Récupération du contenu de la 1ere page contenant la liste des articles
surf_content = get_page_source_content(url='http://www.surfshop.fr/neoprene-72/combinaisons-homme-73#/')

# Extraction des liens individuels des articles grâce aux expressions régulières
output  = get_links(surf_content)

# Retrouver le lien de la page suivante pour la suite des articles suivants
next_page_pattern = 'href="(.*?)">\n\t*<b>Suivant</b>'
next_link=re.findall(next_page_pattern, surf_content)[0]
next_link = 'http://www.surfshop.fr/' + next_link
print(next_link)

output2 = get_links(content=get_page_source_content(url=next_link))

# Concaténation des liens collectés
url_ids = output + output2

#Affichage des 3 premiers et des 3 derniers liens collectés
url_ids[:3], url_ids[-3:]

# Sauvegarde de l'object contenant la liste des URLs
with open('Liste_urls_SurfShop','wb') as file:
    pickle.dump(url_ids, file)


pro_content = get_page_source_content(url='https://glisse-proshop.com/neoprene/combinaisons-homme.html?p=1')
nb_pages = int(re.findall('<span class="product-name">Page suivante \((.*)\)</span>', pro_content)[0].split('/')[1])
print(nb_pages)

proshop_links = []
for num_page in range(1, nb_pages+1):
    content=get_page_source_content(url='https://glisse-proshop.com/neoprene/combinaisons-homme.html?p=%s'%num_page)
    
    #faire attention, les 3 prmiers ne font pas parti
    for link in re.findall('<a href="(.*) class="product-image">', content)[3:]:
        proshop_links.append(link.split('" ')[0])


#save the urls
with open('proshop_urls_list','wb') as file:
    pickle.dump(proshop_links, file)


####################### Partie 2 #############################################

# with open("./Liste_urls_SurfShop", "rb") as input_file:
#     surf_urls_list = pickle.load(input_file)

# save_html_contents(all_links=surf_urls_list, file_name='surfshop_htmls')

# with open("./proshop_urls_list", "rb") as input_file:
#     pro_urls_list = pickle.load(input_file)

# save_html_contents(all_links=pro_urls_list, file_name='proshop_htmls')

####################### Partie 3 #############################################

with open("./surfshop_htmls", "rb") as input_file:
    surfshop_htmls = pickle.load(input_file)

prices_list, thickness_list, brand_list = [], [], []
price_pattern = 'itemprop="price">(.*)</span>'
for page_content in surfshop_htmls[1:]:
    thickness_list.append(get_thickness(page_content))
    prices_list.append(float(re.findall(price_pattern, page_content)[0]))
    
    # Suite à un problème d'encoding, on remplace "&#039" par apostrophe (*'*).
    # .strip() permet de supprimer les espaces vides se trouvant devant le nom de la marque obtenue
    brand_list.append(re.findall(' (.*)\r\n(.*)<span class="text-orange">', page_content)[0][0].strip().replace("&#039;","'"))

surf_shop_df = pd.DataFrame({'Thickness':thickness_list, 'Brand':brand_list, 'Price':prices_list, 'Source':'SurfShop'}).dropna().reset_index(drop=True)
print(surf_shop_df.sample(7, random_state=42))

with open("./proshop_htmls", "rb") as input_file:
    proshop_htmls = pickle.load(input_file)

prices_list, thickness_list, brand_list = [], [], []
for page_content in proshop_htmls:
    thickness_list.append(re.findall('<li>Epaisseur (.*)</li>', page_content)[0][0])
    brand_list.append(re.findall('<img class="image-responsive" alt="(.*)" src=',page_content)[0].capitalize())
    prices_list.append(get_price(page_content))

pro_shop_df = pd.DataFrame({'Thickness':thickness_list, 'Brand':brand_list, 'Price':prices_list, 'Source':'ProShop'}).dropna().reset_index(drop=True)
print(pro_shop_df.sample(7, random_state=42))

final_data = pd.concat([surf_shop_df, pro_shop_df], ignore_index=True)
print(final_data.sample(7, random_state=1))


# Analyse econometrique
final_data["from_surf"] = np.where(final_data.Source == "SurfShop", 1, 0)
final_data.sample(3, random_state=2)

Y = final_data.Price
X = sm.add_constant(final_data[["Thickness", "from_surf"]])
model = sm.OLS(endog=Y, exog=X.astype(float))
res = model.fit()
print(res.summary())