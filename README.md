# Web Scrapping

Il s'agit de deux différents projets de Web Scrapping utilisant des librairies différentes de Python. Le web Scrapping est accompagné par l'utilisation de packages sur l'analyse de données économétrique. Le projet est composé de deux dossiers essentiels :


- **Using regular expressions** : Contient un projet de web scrapping utilisant uniquement des expressions régulières pour l'extraction d'informations sur un site marchand de Surf appelé *surfshop*.

- **Using Selenium** : Il s'agit d'un autre projet utilisant la librairie selenium et beautifulSoup de python pour extraire les données sur les site de vente *mizuno* et *boutiquemarathon*.

Dans tous les projets, un notebook bien détaillé explique toutes les étapes des processus d'extraction et d'analyse des données.
